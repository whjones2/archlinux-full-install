###################################################################
#  Author: William H. Jones II
# 
#  NOTE: Please read this carefully prior to runnigng this script.
#   The following will be set by this script:
#    timezone (line 21)
#    locale (lines 27-29)
#    hostname (lines 58-65)
#    root password (line 68-70)
#    non-root user account (lines 72-104)
#
#  NOTE: Feel free to edit this script as desired
####################################################################

#!/usr/bin/env bash

# set some colors
CNT="[\e[1;36mNOTE\e[0m]"
COK="[\e[1;32mOK\e[0m]"
CER="[\e[1;31mERROR\e[0m]"
CAT="[\e[1;37mATTENTION\e[0m]"
CWR="[\e[1;35mWARNING\e[0m]"
CAC="[\e[1;33mACTION\e[0m]"
INSTLOG="install.log"

# Set the time zone for Honolulu
ln -s /usr/share/zoneinfo/Pacific/Honolulu /etc/localtime

# Set the hardware clock to the system clock
hwclock --systohc --utc

# Set the locale
sed -i '171s/.//' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 >> /etc/locale.conf

# Set desired global vim values
echo "set number" >> /etc/vimrc
echo "set relativenumber" >> /etc/vimrc
echo "syntax on" >> /etc/vimrc
echo "set clipboard=unnamed" >> /etc/vimrc
echo "set noerrorbells" >> /etc/vimrc
echo "set tabstop=4 softtabstop=4" >> /etc/vimrc
echo "set shiftwidth=4" >> /etc/vimrc
echo "set expandtab" >> /etc/vimrc
echo "set smartindent" >> /etc/vimrc
echo "set scrolloff=8" >> /etc/vimrc
echo "set incsearch" >> /etc/vimrc

# Set desited pacman.conf file values
sed -i '33s/.//' /etc/pacman.conf
sed -i '36s/.//' /etc/pacman.conf
sed -i '37s/.//' /etc/pacman.conf
sed -i '37s/ParallelDownloads = 5/ParallelDownloads = 10/' /etc/pacman.conf
sed -i '37a ILoveCandy' /etc/pacman.conf
sed -i '94s/.//' /etc/pacman.conf
sed -i '95s/.//' /etc/pacman.conf

# Synd the mirrors
pacman -Syy

# Prompt the user for the hostname for this build.
echo ""
read -p "Please enter the desired hostname: " HostName

# Apply the new hostname to /etc/hostname and /etc/hosts
echo $HostName >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1   localhost" >> /etc/hosts
echo "127.0.1.1 $HostName.localdomain    $HostName" >> /etc/hosts

# Confirm successful creation of new user account
echo $HostName " has been applied successfully."

# Set the root password
echo ""
/bin/echo -e "\e[1;32mEnter a new password for the root account.\e[0m"
passwd root

## Create a non-root user
echo ""
read -r -p "Would you like to create a regular user? [y/n] " input
case $input in

    # If yes, prompt the user for the desired username
        [yY][eE][sS]|[yY])
        read -p "Please enter the desired username: " USR

        # Create a home directory and assign basid user environmental variables to the new user
        useradd -m -G wheel,power,storage,audio,video -s /bin/bash $USR
        echo $USR "has been successfully created."
        echo ""

        # Create a password for the new user
        /bin/echo -e "\e[1;32mEnter a new password for the\e[0m" $USR "\e[1;32muser account.\e[0m"
        passwd $USR

        # Set sudo permission for non-root user
        echo "$USR ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/$USR
        ;;

    # If no, proceed on to the next line
        [n/N][o/O][n/N])
        echo "No"
        ;;

    # If any other response, exit the case statement
        *)
        echo "Invalid input..."
        exit 1
        ;;
esac

# Confirm successful creation of new user account
echo ""
echo $USR " has been created successfully."
echo ""

# Packages to be installed go here
pacman -S efibootmgr man networkmanager wpa_supplicant bash-completion amd-ucode xf86-video-amdgpu dialog wget unrar firefox p7zip neofetch yajl networkmanager-openvpn multilib-devel dosfstools mtools iw wireless_tools ntfs-3g tmux reflector lsb-release xdg-utils xdg-user-dirs vim htop bashtop os-prober dmidecode

# Install bootloader
echo ""
PS3='Select a bootloader to install: '
bootloader=("GRUB" "SystemD")
select fav in "${bootloader[@]}"; do
    case $fav in
        "GRUB")
            # Install the GRUB bootloader
            echo ""
            echo "GRUB is the chosen bootloader."
            pacman -S grub
            grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
            os-prober
            sed -i '7a GRUB_DISABLE_OS_PROBER=false' /etc/default/grub
            grub-mkconfig -o /boot/grub/grub.cfg
            break
        ;;
        "SystemD")
            # Install the systemd bootloader
            echo ""
            echo "SystemD is the chosen bootloader."
            echo ""
            read -p "Press any key to continue ..."
            echo ""
            bootctl --path=/boot/ install
            echo ""       

            cd /boot/loader     

           # Edit the boot/efi loader configuration file
            echo "default   arch.conf" > loader.conf
            echo "timeout   5" >> loader.conf
            echo "editor    0" >> loader.conf
            # echo "console-mode 1" >> /boot/efiefi/loader/loader.conf

            # Verify the steps above
            cat loader.conf
            echo ""
            read -p "Here is the updated /boot/efi/loader/loader.conf file."
            echo ""
            echo "Press any key to continue ..."
            echo ""

            cd entries

            # Edit the default Arch boot/efi configuration file
            touch arch.conf

            # Edit the arch.conf file
            echo "title     SystemD Boot Arch Project" > arch.conf 
            echo "linux     /vmlinuz-linux" >> arch.conf 
            echo "initrd        /amd-ucode.img" >> arch.conf 
            echo "initrd        /initramfs-linux.img" >> arch.conf 

            # Get the UUID for the Arch Linux root partition and append it to the default Arch boot/efi configuration file
            # Specify the Arch Linux partition
            #
            echo ""
            '
            PS3="Select the Arch Linux install partition: "
            part=("sda2" "vda2" "nvme0n1p2" "nvme0n1p5")
            select fav in "${part[@]}"; do
                case $fav in
                    "sda2")
                        arch_part=/dev/sda2
                        echo $arch_Part "is the Arch Linux installation partition."
                        break
                    ;;
                    "vda2")
                        arch_part=/dev/vda2
                        echo $arch_Part "is the Arch Linux installation partition."
                        break
                    ;;
                    "nvme0n1p2")
                        arch_part=/dev/nvme0n1p2
                        echo $arch_Part "is the Arch Linux installation partition."
                        break
                    ;;
                    "nvmd0n3p5")
                        arch_part=/dev/nvme0n1p5
                        echo $arch_Part "is the Arch Linux installation partition."
                        break
                    ;;
                    *)  
                        # For invalid keypress
                        echo "invalid option"
                        exit 1
                    ;;
                esac
            done
            '
            read -p "Which partition contains the Arch Linux installation (include /dev/): ? " arch_part
            echo ""
            echo $arch_part " is the Arch Linux partition."

            # Append the UUID for the Arch Linux partition to the default Arch boot/efi configuration file
            echo "options root=UUID=$(blkid -s UUID -o value $arch_part) quiet rw" >> arch.conf
            #echo "options root=/dev/vda2 quiet rw" >> arch.conf

            # Verify the steps above
            echo ""
            cat arch.conf
            echo ""
            read -p "Here is the updated /boot/loader/entries/arch.conf."  
            echo ""
            read -p "Press any key to continue ..."

            # Update bootctl with the new entries
            #bootctl --path=/boot update
            #echo ""
            #echo "bootctl has been updated."

            # Verify successful bootctl update
            bootctl --path=/boot status
            break
        ;;        
        *)  
            # For invalid keypress
            echo "invalid option"
            exit 1
        ;;
    esac
done

# Enable necessary services
#systemctl enable NetworkManager.service
#systemctl enable fstrim.timer

# Install the Gnome default
#pacman -S gnome gnome-extra gdm
#systemctl enable gdm.service

# Invoke neofetch to verify proper installation
neofetch

NOTE: At this point, edit the mkinitcpio file
vim /etc/mkinitcpio.conf

/bin/echo -e "\e[1;32mDone! Type exit, umount -a, then reboot.\e[0m"
