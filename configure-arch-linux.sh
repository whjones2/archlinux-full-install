###################################################################
#  Author: William H. Jones II
#  Date: 28 December 2021
# 
#  NOTE: Please read this carefully prior to running this script.
#  NOTE: This script allows the user to install the desktop
#         environment of their choice and a full load of apps.
#  NOTE: Feel free to comment out any unnecessary lines
####################################################################

#!/usr/bin/env bash

# If on WiFi, get WiFi IP address; otherwise, exit the nmtui app
# Prompt user to verify if on WiFi
echo ""
read -r -p "Are you using WiFi? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
# If on WiFi, run Network Manager UI to connect to wireless AP
   nmtui
   ;;
    [nN][oO]|[nN])
# If on Ethernet, exit to next command
 echo "No"
       ;;
    *)
 echo "Invalid input..."
 exit 1
 ;;
esac

echo ""
# Verify IP address
ip -c a
read -p "Press any key to continue ..."

echo ""
# Create new mirrorlist
read -r -p "Would you like to sync the mirrors (make take a long time)? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
 # Use reflector to get the best mirrors
 reflector -l 100 -f 50 --sort rate --verbose --save /etc/pacman.d/mirrorlist; more /etc/pacman.d/mirrorlist
 read -p "Press any key to continue ...";;
    [nN][oO]|[nN])
# If no, exit to next command
 echo "No"
       ;;
    *)
 echo "Invalid input..."
 exit 1
 ;;
esac

# Sync the mirrors
sudo pacman -Syy

# Install paru AUR helper
cd ~/.config
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si;cd ..

# Install pamac-aur
paru -S pamac-aur


echo ""

# Install additional packages
paru -S flameshot meld telegram-desktop discord simplescreenrecorder scrot pulseaudio pulseaudio-alsa pavucontrol alsa-firmware alsa-lib alsa-utils alsa-plugins gstreamer gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly playerctl volumeicon pulseaudio-bluetooth bluez bluez-libs bluez-utils blueberry blueman 

sudo systemctl enable --now bluetooth.service

sudo sed -i 's/'#AutoEnable=false'/'AutoEnable=true'/g' /etc/bluetooth/main.conf

paru -S cups cups-pdf ghostscript gsfonts gutenprint gtk3-print-backends libcups system-config-printer hplip

sudo systemctl enable cups

echo ""
read -r -p "Would you like to install Samba)? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
 		# Use reflector to get the best mirrors
 		paru -S samba gvfs-smb
 	
 		# Copy the Samba configuration file
 		sudo cp /etc/samba/smb.conf.arcolinux /etc/samba/smb.conf
 	
 		# Create Samba username
 		read -p "Enter the desired Samba login: " input
 		sudo smbpasswd -a $input
 	
 		# Enable Samba
 		sudo systemctl enable smb.service
 		sudo systemctl enable nmb.service
	;;
    [nN][oO]|[nN])
		# If no, exit to next command
 		echo "Samba will not be installed."
    ;;
    *)
 		echo "Invalid input..."
 		exit 1
 	;;
esac

echo ""
# Is this build for a notebook computer?
read -r -p "Is this build for a notebook computer? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
		# Install tlp to optimize battery life
		paru -S tlp
	
		# Enable tlp
		sudo systemctl enable tlp.service
	;;
    [nN][oO]|[nN])
		# If no, exit to next command
 		echo "No"
       	;;
    *)
 		echo "Invalid input..."
 		exit 1
 	;;
esac

paru -S avahi nss-mdns gvfs-smb

sudo sed -i 's/files mymachines myhostname/files mymachines/g' /etc/nsswitch.conf
sudo sed -i 's/\[\!UNAVAIL=return\] dns/\[\!UNAVAIL=return\] mdns dns wins myhostname/g' /etc/nsswitch.conf
sudo systemctl enable avahi-daemon.service

paru -S gimp inkscape kdenlive obs-studio guvcview gthumb evince libreoffice-fresh vlc handbrake dconf-editor unace unrar zip unzip sharutils uudeview arj file-roller downgrade inxi dmenu imagemagick

paru -S awesome-terminal-fonts adobe-source-sans-pro-fonts cantarell-fonts noto-fonts ttf-bitstream-vera ttf-dejavu ttf-droid ttf-hack ttf-inconsolata ttf-liberation ttf-roboto ttf-ubuntu-font-family tamsyn-font

# Reboot the computer
/bin/echo -e "\e[1;32mREBOOTING IN 5 SECONDS\e[0m"
sleep 5
sudo reboot

