# archlinux-full-install

This project contains scripts to install Arch Linux, to include Desktop Environments and Window Managers.  If you are not in the Honolulu area and/or would like to use other than US English localization, please feel free to edit  the arch-install-grub.sh file or the arch-install-systemd.sh file.
